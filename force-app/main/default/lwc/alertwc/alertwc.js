import { LightningElement, track, wire } from 'lwc';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';

export default class Alertwc extends LightningElement {
    @wire(CurrentPageReference)
    pageRef;

    @track alertMessage = undefined;

    /**
     * Register listener for alerts
     */
    connectedCallback() {
        registerListener('newAlert', this.handleAlert, this);
    }    

    /**
     * Remove all listeners
     */
    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    /**
     * Handle received alert
     * @param {Object} alr 
     */
    handleAlert(alr) {
        if (alr && alr.msg) {
            this.alertMessage = alr;
        }
    }

    /**
     * Reset alertMessage property
     */
    closeAlert() {
        this.alertMessage = undefined;
    }

    get isError() {
        return this.alertMessage.type === 'error';
    }

    get isWarning() {
        return this.alertMessage.type === 'warning';
    }

    get isInfo() {
        return !this.alertMessage.type || this.alertMessage.type === 'info';
    }
}