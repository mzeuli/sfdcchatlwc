import {
    createElement
} from "lwc";
import ChatUsersPresenceWc from "c/chatUsersPresenceWc";
import updateLastTimeSeen from "@salesforce/apex/Chat_CtrUsersPresence.updateLastTimeSeen";

jest.mock("@salesforce/apex/Chat_CtrUsersPresence.updateLastTimeSeen");

describe("chatUsersPresence", () => {

    beforeEach(() => {
        jest.useFakeTimers();
    });

    afterEach(() => {
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }

        jest.clearAllMocks();
    });

    it("refreshes user presence when connected", () => {

        const el = createElement("c-chat-users-presence-wc", {
            is: ChatUsersPresenceWc
        });
        
        el.roomId = "123";

        document.body.appendChild(el);

        return Promise.resolve().then(() => {
            expect(updateLastTimeSeen).toHaveBeenCalled();
        });
    });

    it("starts user presence refresh interval when connected", () => {

        const el = createElement("c-chat-users-presence-wc", {
            is: ChatUsersPresenceWc
        });

        el.roomId = "123";

        document.body.appendChild(el);

        return Promise.resolve().then(() => {
            expect(setInterval).toHaveBeenCalledTimes(1);
        });
    });

    it("remove user presence refresh interval when disconnected", () => {

        const el = createElement("c-chat-users-presence-wc", {
            is: ChatUsersPresenceWc
        });

        el.roomId = "123";

        document.body.appendChild(el);
        document.body.removeChild(el);

        return Promise.resolve().then(() => {
            expect(clearInterval).toHaveBeenCalledTimes(1);
        });
    });
});