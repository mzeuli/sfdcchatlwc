import { LightningElement, api, wire } from 'lwc';
import Id from '@salesforce/user/Id';
import { getRecord } from 'lightning/uiRecordApi';

const userFields = [
    'User.Id',
    'User.Name'
];

export default class ChatRoomWc extends LightningElement {

    /**
     * Current user's ID
     */
    userId = Id;

    /**
     * Current user record
     */
    @wire(getRecord, { recordId: '$userId', userFields })
    user;

    /**
     * Chat room id
     */
    @api roomId;

    /**
     * Size of chat pane that displays messages. Value is in pixel
     */
    @api chatPaneSize = 300;

    /**
     * Checks whether this component is ready to display or not.
     * Component is ready to display when both user and roomId have been defined
     */
    get isVisible() {
        console.log('dummy log')
        return this.roomId && this.user && this.user.data;
    }
}