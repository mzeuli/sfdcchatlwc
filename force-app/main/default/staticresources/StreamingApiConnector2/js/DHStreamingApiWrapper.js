(
    function (root, factory) {
        if (typeof exports === 'object') {
            // CommonJS.
            module.exports = factory();
        } else if (typeof define === 'function' && define.amd) {
            // AMD.
            define([], factory);
        } else {
            // Globals.
            root.StreamingApiWrapper = root.StreamingApiWrapper || {};
            root.StreamingApiWrapper = factory();
        }
    }(
        this,
        function () {
            /**
             *   Constructor, receives a required object containing configuration options.
             *   Valid options are:
             *       token                       [required]: API access token
             *       cometdURL                   [required]: cometd server url.
             *       appendMessageTypeToURL      [optional]: boolean that specify whether cometD should append message type to
             *                                               URL or not. Default is false.
             *       websocketEnabled            [optional]: boolean that specify if cometd should use websocket or not.
             Default is false.
             *       debug                       [optional]: boolean that specify if to enable debug logs or not. Default is false.
             */
            return function (configs) {

                // checks for valid configuration
                if (!configs) {
                    throw new Error("Required configurations are missing");
                }

                if (!configs.hasOwnProperty("cometdURL")) {
                    throw new Error("Missing required option cometdURL");
                }

                if (!configs.hasOwnProperty("token")) {
                    throw new Error("Missing required option token");
                }

                _log("DEBUG: Configs are: ", configs);

                // current topic
                var _channel = null;

                // set to true after first connection
                var _connected = false;

                // set to true after a successful subscription
                var _topicsubscription = false;

                // cometd client instance
                var _cometd;

                /**
                 *   Connection listener. Used to debug connection flow.
                 */
                function _metaConnectListener(message) {
                    var wasConnected = _connected;
                    _connected = message.successful;
                    if (!wasConnected && _connected) {
                        _log('DEBUG: Connection Successful: ', message);
                    } else if (wasConnected && !_connected) {
                        _log('DEBUG: Disconnected from the server: ', message);
                    }
                }

                /**
                 *   Handshake listener. Used to debug handshake flow.
                 */
                function _metaHandshakeListener(message) {
                    if (message.successful) {
                        _log('DEBUG: Handshake Successful: ', message);
                    } else {
                        _log('DEBUG: Handshake Unsuccessful: ', message);
                    }
                }

                /**
                 *   Disconnection listener. Used to debug disconnection flow.
                 */
                function _metaDisconnectListener(message) {
                    _log('DEBUG: /meta/disconnect message: ', message);
                }

                /**
                 *   Subscription listener. Used to debug subscription flow.
                 */
                function _metaSubscribeListener(message) {
                    if (message.successful) {
                        _log('DEBUG: Subscribe Successful ' + _channel + ': ', message);
                    } else {
                        _log('DEBUG: Subscribe Unsuccessful ' + _channel + ': ', message);
                    }
                };

                /**
                 *   Unsubscribe listener. Used to debug unsubscribe flow.
                 */
                function _metaUnSubscribeListener(message) {
                    if (message.successful) {
                        _log('DEBUG: Unsubscribe Successful ', message);
                    } else {
                        _log('DEBUG: Unsubscribe Unsuccessful ', message);
                    }
                };

                /**
                 *   Unsuccessful listener. Used for debug.
                 */
                function _metaUnSucessfulListener(message) {
                    _log('DEBUG:  /meta/unsuccessful Error: ', message);
                };

                /**
                 *   If debug is enable will print messages in console.
                 */
                function _log(text, details) {
                    if (!!configs.debug) {
                        console.log(text, details);
                    }
                }

                /* ********** EXPOSED API FUNCTIONS ********** */

                /**
                 *   Connect to CometD Salesforce endpoint.
                 *   @param errfcb   Error first callback to call after connection attempt.
                 */
                function connect(errfcb) {
                    errfcb = (typeof errfcb === "function") ? errfcb : function () {
                    };

                    _cometd = new org.cometd.CometD();
                    _cometd.websocketEnabled = !!configs.websocketEnabled;

                    _cometd.configure({
                        url: configs.cometdURL,
                        maxBackoff: 40000,
                        requestHeaders: {
                            Authorization: 'OAuth ' + configs.token
                        },
                        appendMessageTypeToURL: !!configs.appendMessageTypeToURL
                    });

                    _cometd.addListener('/meta/connect', _metaConnectListener);

                    _cometd.addListener('/meta/handshake', _metaHandshakeListener);

                    _cometd.addListener('/meta/disconnect', _metaDisconnectListener);

                    _cometd.addListener('/meta/subscribe', _metaSubscribeListener);

                    _cometd.addListener('/meta/unsubscribe', _metaUnSubscribeListener);

                    _cometd.addListener('/meta/unsuccessful', _metaUnSucessfulListener);

                    _cometd.handshake(function (h) {
                        _connected = h.successful;
                        errfcb(!h.successful);
                    });

                    return this;
                }

                /**
                 *   Subscribe to specified topic.
                 *   @param topic    Topic to subscribe to.
                 *   @param cb       Callback to invoke. First passed parameter will be the data attached to the event received.
                 */
                function subscribe(topic, cb) {
                    cb = (typeof cb === "function") ? cb : function () {
                    };

                    if (!_connected) {
                        throw new Error("Cannot subscribe due to unsuccessful connection");
                    }

                    if (!topic) {
                        throw new Error("No valid topic provided");
                    }

                    _channel = topic;
                    _topicsubscription = _cometd.subscribe(_channel, function (message) {
                        cb(message.data);
                    });

                    return this;
                }

                /**
                 *   Unsubscribe from current topic.
                 */
                function unsubscribe() {
                    if (_topicsubscription) {
                        _cometd.unsubscribe(_topicsubscription);
                    }

                    _topicsubscription = null;

                    return this;
                }

                /**
                 * Disconnect current client
                 */
                function disconnect() {
                    _cometd.disconnect();
                }

                // Public API
                return {
                    connect: connect,
                    disconnect: disconnect,
                    unsubscribe: unsubscribe,
                    subscribe: subscribe
                };
            }
        }
    )
);