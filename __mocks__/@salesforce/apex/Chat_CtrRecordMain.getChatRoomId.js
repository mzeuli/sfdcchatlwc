const roomId = '123';

const getChatRoomId = jest.fn(
    recordId => new Promise((resolve, reject) => {
        if (recordId) {
            resolve(roomId);
        } else {
            reject();
        }
    })
);

export default getChatRoomId;