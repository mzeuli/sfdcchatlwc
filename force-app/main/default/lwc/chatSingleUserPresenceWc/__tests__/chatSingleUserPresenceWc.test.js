import { createElement } from "lwc";
import ChatSingleUserPresenceWc from "c/chatSingleUserPresenceWc";

describe("chatSingleUserPresnece", () => {

    const userPresenceRecord = {
        Id: "123",
        CreatedBy: {
            Id: "u123",
            Name: "Marco Whatever"
        }
    };

    it("displays correctly when user presence is set and hide is false", () => {
        const el = createElement("c-chat-single-user-presence-wc", {
            is: ChatSingleUserPresenceWc
        });

        document.body.appendChild(el);

        el.userPresence = userPresenceRecord;

        return Promise.resolve().then(() => {
            const liEl = el.shadowRoot.querySelector("li");

            expect(liEl).not.toBeNull();
        });
    });

    it("does not display when user presence is not set and hide is false", () => {
        const el = createElement("c-chat-single-user-presence-wc", {
            is: ChatSingleUserPresenceWc
        });

        document.body.appendChild(el);

        return Promise.resolve().then(() => {
            const liEl = el.shadowRoot.querySelectorAll("li");
            expect(liEl.length).toBeFalsy();
        });
    });

    it("does not display when user presence is set and hide is true", () => {
        const el = createElement("c-chat-single-user-presence-wc", {
            is: ChatSingleUserPresenceWc
        });

        document.body.appendChild(el);

        el.userPresence = userPresenceRecord;
        el.hide = true;

        return Promise.resolve().then(() => {
            const liEl = el.shadowRoot.querySelectorAll("li");
            expect(liEl.length).toBeFalsy();
        });
    });

    afterEach(() => {
        while(document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
    });

});