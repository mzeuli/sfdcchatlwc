import { LightningElement, api } from 'lwc';

export default class ChatSingleUserPresenceWc extends LightningElement {
    /**
     * User presence record to display
     */
    @api userPresence = undefined;

    /**
     * Show or hide the user presence record
     */
    @api hide = false;

    /**
     * Returns user's first and last name related to current user presence record
     */
    @api
    get userName() {
        return (
            this.userPresence &&
            this.userPresence.CreatedBy
        ) ? this.userPresence.CreatedBy.Name : "";
    } 

    /**
     * Retruns true if component is visibile.
     * Component is visibile if userPresence is set and hide is false
     */
    get visible() {
        return !this.hide && this.userPresence;
    }
}