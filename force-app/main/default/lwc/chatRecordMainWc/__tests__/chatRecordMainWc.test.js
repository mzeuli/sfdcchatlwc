import {
    createElement
} from 'lwc';
import ChatRecordMainWc from 'c/chatRecordMainWc';
import {
    registerTestWireAdapter,
    registerLdsTestWireAdapter
} from '@salesforce/wire-service-jest-util';
import {
    CurrentPageReference
} from "lightning/navigation";
import getChatRoomId from '@salesforce/apex/Chat_CtrRecordMain.getChatRoomId';
import {
    getRecord
} from 'lightning/uiRecordApi';

jest.mock('@salesforce/apex/Chat_CtrRecordMain.getChatRoomId');

describe("chatRecordMain", () => {

    afterEach(() => {
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }

        jest.clearAllMocks();
    });

    const mockPageRef = {
        type: "standard__component",
        attributes: {
            componentName: "c__MyLightningComponent"
        },
        state: {
            c__counter: "5"
        }
    };

    registerLdsTestWireAdapter(getRecord);

    const getPageRefWireAdapter = registerTestWireAdapter(CurrentPageReference);

    it('renders chat room correctly when connected and recordId is not null', (done) => {

        const el = createChatRecordMainElement();
        el.recordId = '123';

        document.body.appendChild(el);

        // wait for getChatRoomId Promise to resolve
        process.nextTick(() => {
            const grid = el.shadowRoot.querySelector("c-chat-room-wc");
            expect(getChatRoomId).toHaveBeenCalledTimes(1);
            expect(grid).not.toBeNull();
            done();
        });
    });

    it('does not renders chat room if roomId is undefined', (done) => {

        getChatRoomId.mockImplementationOnce(() => Promise.reject());

        const el = createChatRecordMainElement();
        el.recordId = '123';
        
        document.body.appendChild(el);

        process.nextTick(() => {
            const grid = el.shadowRoot.querySelector("c-chat-room-wc");
            expect(getChatRoomId).toHaveBeenCalledTimes(1);
            expect(grid).toBeNull();
            done();
        });
    });

    function createChatRecordMainElement() {

        const el = createElement('c-chat-record-main-wc', {
            is: ChatRecordMainWc
        });

        getPageRefWireAdapter.emit(mockPageRef);

        return el;
    }

});