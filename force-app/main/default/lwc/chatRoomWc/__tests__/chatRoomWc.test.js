import {
    createElement
} from 'lwc';
import ChatRoomWc from 'c/chatRoomWc';
import {
    getRecord
} from 'lightning/uiRecordApi';
import {
    registerLdsTestWireAdapter,
    registerTestWireAdapter
} from '@salesforce/wire-service-jest-util';
import {
    CurrentPageReference
} from "lightning/navigation";

jest.mock('c/pubsub', () => {
    return {
        registerListener: function () {},
        unregisterListener: function () {},
        unregisterAllListeners: function () {},
        fireEvent: function () {}
    };
});

describe('chatRoom', () => {

    const mockPageRef = {
        type: "standard__component",
        attributes: {
            componentName: "c__MyLightningComponent"
        },
        state: {
            c__counter: "5"
        }
    };

    const getPageRefWireAdapter = registerTestWireAdapter(CurrentPageReference);

    const userRecord = {
        fields: {
            Id: {
                value: '123'
            },
            Name: {
                value: 'Marco'
            }
        }
    };

    const getUserRecordWireAdapter = registerLdsTestWireAdapter(getRecord);

    afterEach(() => {
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
    });

    it('displays correctly when user and roomId are defined', () => {

        const el = createChatRoomElement();
        document.body.appendChild(el);
        el.roomId = '123';
        getUserRecordWireAdapter.emit(userRecord);

        return Promise.resolve().then(() => {
            const grid = el.shadowRoot.querySelector(".slds-grid");
            expect(grid.childNodes.length).toBe(4);
        });
    });

    it('does not display when roomId is not defined', () => {

        const elWithoutRoom = createChatRoomElement();
        document.body.appendChild(elWithoutRoom);
        getUserRecordWireAdapter.emit(userRecord);

        const elWithoutUser = createChatRoomElement();
        document.body.appendChild(elWithoutUser);
        elWithoutUser.roomId = '123';

        return Promise.resolve().then(() => {
            const grid1 = elWithoutRoom.shadowRoot.querySelector(".slds-grid");
            const grid2 = elWithoutUser.shadowRoot.querySelector(".slds-grid");
            expect(grid1.childNodes.length).toBeFalsy();
            expect(grid2.childNodes.length).toBeFalsy();
        });
    });

    function createChatRoomElement() {
        const el = createElement("c-chat-room-wc", {
            is: ChatRoomWc,
        });

        // this is because chatRoomWc renders an instance of alertwc
        getPageRefWireAdapter.emit(mockPageRef);

        return el;
    }
});