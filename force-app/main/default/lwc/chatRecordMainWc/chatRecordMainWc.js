import {
    LightningElement,
    api,
    track,
    wire
} from 'lwc';
import {
    CurrentPageReference
} from "lightning/navigation";
import getChatRoomId from '@salesforce/apex/Chat_CtrRecordMain.getChatRoomId';

export default class ChatRecordMainWc extends LightningElement {
    /**
     * Current record's id
     */
    @api recordId;

    /**
     * Current chat room's id
     */
    @track roomId;

    @wire(CurrentPageReference)
    pageRef;

    connectedCallback() {
        getChatRoomId({recordId: this.recordId})
            .then(result => {
                this.roomId = result;
            }).catch(error => {
                // todo: how to notify this to users? Toasts?
                console.log(error);
            });
    }
}