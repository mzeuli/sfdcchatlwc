const updateLastTimeSeen = jest.fn(
  roomId => new Promise((resolve, reject) => {
      roomId ?
      resolve() :
      reject()
  })
);

export default updateLastTimeSeen;