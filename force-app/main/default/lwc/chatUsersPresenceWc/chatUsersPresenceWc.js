/* eslint-disable @lwc/lwc/no-async-operation */
import {
    LightningElement,
    api,
    track
} from 'lwc';

import
updateLastTimeSeen
from "@salesforce/apex/Chat_CtrUsersPresence.updateLastTimeSeen";

export default class ChatUsersPresenceWc extends LightningElement {

    /**
     * Chat room 's id
     */
    @api roomId;

    /**
     * Maximum height in pixel for the popover
     */
    @api popoverMaxHeight;

    /**
     * Users presence related to current chat room
     */
    @track usersPresence = [];

    /**
     * Search text to filter users
     */
    @track searchText = '';


    /**
     * Interval's id that refreshes user presence
     */
    intervalId;

    connectedCallback() {
        // immediately update user's presence record
        this.updateUserPresence();

        // schedules user presence refresh
        this.intervalId = setInterval(() => {
            this.updateUserPresence();
        }, 5000);
    }

    /**
     * Called when user enter a search term, updates the value of searchText and
     * triggers the users filter logic
     * @param {Event} event 
     */
    setSearchText(event) {
        this.searchText = event.target.value;
        this.filterUsers();
    }

    /**
     * Based on searchText value show or hide users from the list
     */
    filterUsers() {
        // todo: implement this
    }

    disconnectedCallback() {
        if (this.intervalId) clearInterval(this.intervalId);
    }

    updateUserPresence() {
        updateLastTimeSeen(this.roomId)
            .then(result => {
                // nothing to do
                console.log("User presence updated, result is: ", result);
            })
            .catch(error => {
                // maybe show an error? Problem is this function is called multiple times
                console.log("Error while updating user presence, error is: ", error);
            });
    }
}