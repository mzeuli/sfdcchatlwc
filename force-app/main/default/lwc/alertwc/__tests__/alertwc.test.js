import {
    registerTestWireAdapter
} from "@salesforce/wire-service-jest-util";
import {
    createElement
} from "lwc";
import Alertwc from "c/alertwc";
import {
    fireEvent
} from "c/pubsub";
import {
    CurrentPageReference
} from "lightning/navigation";


describe("alert", () => {

    const mockPageRef = {
        type: "standard__component",
        attributes: {
            componentName: "c__MyLightningComponent"
        },
        state: {
            c__counter: "5"
        }
    };

    const getPageRefWireAdapter = registerTestWireAdapter(CurrentPageReference);

    it('displays error correctly', () => {
        const msg = "An error message";

        const element = createElement('c-alert', {
            is: Alertwc
        });

        getPageRefWireAdapter.emit(mockPageRef);

        // Add component to DOM (jsdom) so it renders, goes through creation lifecycle
        document.body.appendChild(element);

        fireEvent(mockPageRef, 'newAlert', {
            msg,
            type: "error"
        });

        return Promise.resolve().then(() => {
            const alertContainer = element.shadowRoot.querySelectorAll(".slds-notify");
            const errorContainer = element.shadowRoot.querySelector(".slds-theme_error h2");
            expect(alertContainer.length).toBe(1);
            expect(errorContainer).not.toBeNull();
            expect(errorContainer.textContent).toBe(msg);
        });
    });

    it('displays warning correctly', () => {
        const msg = "A warning message";

        const element = createElement('c-alert', {
            is: Alertwc
        });

        getPageRefWireAdapter.emit(mockPageRef);

        // Add component to DOM (jsdom) so it renders, goes through creation lifecycle
        document.body.appendChild(element);

        fireEvent(mockPageRef, 'newAlert', {
            msg,
            type: "warning"
        });

        return Promise.resolve().then(() => {
            const alertContainer = element.shadowRoot.querySelectorAll(".slds-notify");
            const warnContainer = element.shadowRoot.querySelector(".slds-theme_warning h2");
            expect(alertContainer.length).toBe(1);
            expect(warnContainer).not.toBeNull();
            expect(warnContainer.textContent).toBe(msg);
        });
    });

    it('displays info correctly', () => {
        const msg = "An info message";

        const element = createElement('c-alert', {
            is: Alertwc
        });

        getPageRefWireAdapter.emit(mockPageRef);

        // Add component to DOM (jsdom) so it renders, goes through creation lifecycle
        document.body.appendChild(element);

        fireEvent(mockPageRef, 'newAlert', {
            msg,
            type: "info"
        });

        return Promise.resolve().then(() => {
            const alertContainer = element.shadowRoot.querySelectorAll(".slds-notify");
            const infoContainer = element.shadowRoot.querySelector(".slds-theme_info h2");
            expect(alertContainer.length).toBe(1);
            expect(infoContainer).not.toBeNull();
            expect(infoContainer.textContent).toBe(msg);
        });
    });

    it('displays info correctly when type is undefined', () => {
        const msg = "An info message";

        const element = createElement('c-alert', {
            is: Alertwc
        });

        getPageRefWireAdapter.emit(mockPageRef);

        // Add component to DOM (jsdom) so it renders, goes through creation lifecycle
        document.body.appendChild(element);

        fireEvent(mockPageRef, 'newAlert', {
            msg
        });

        return Promise.resolve().then(() => {
            const alertContainer = element.shadowRoot.querySelectorAll(".slds-notify");
            const infoContainer = element.shadowRoot.querySelector(".slds-theme_info h2");
            expect(alertContainer.length).toBe(1);
            expect(infoContainer).not.toBeNull();
            expect(infoContainer.textContent).toBe(msg);
        });
    });

    afterEach(() => {
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
    })
})